package main 

import (
	"fmt"
	"math"
)
 func main () {
	fmt.Println(math.Pi)
}

// Name is exported if it begins with a capital letter. When Importing a package, you can refer only to its exported 
// names. Any "unexported" name are not accessible from outside the package.

