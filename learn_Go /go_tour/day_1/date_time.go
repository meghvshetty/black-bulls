package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Welcome to the time date program")
	fmt.Println("Time now is", time.Now())
}
