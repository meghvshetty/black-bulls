package main

import "fmt"

//define a entry point
func main() {
	//creating variable
	var name string = "value1" // to give value
	var name_2 string          // Give value later
	name_2 = "value2"
	var number uint16 = 260 //Explicit variable
	var number_2 = 5.43     //Implicit variable
	number_3 := 4576
	fmt.Println(name, name_2, number)
	fmt.Println("Hello World!")
	fmt.Printf("%T", number_2) //finding the "type"
	fmt.Printf("%T", number_3)
}
