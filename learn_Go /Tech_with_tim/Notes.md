# Chapter Getting started
In go we have to define a package at the start of the program.  
```
package main
```
We will import fmt as to display/ print on the screen.
```
import "fmt"
``` 

To run a GO Program 
```
$ go run tutorial.go
```

To build GO program *Binary file*
```
$ go build tutorial.go
```
# Chapter Data_types
**Variable:** are used to store information to be referenced and manipulated in computer program. They also provide a way of labeling data with a descriptive data with a descriptive name, So our prograns can be understood more clearly by the reader. It is helpful to think of variable as containers that hold information. Thier sole pupose is to label and store data in memory. This data can be then be used throughtout your program.

**static type variable declaration:** A static type varaible declaration provides assurace to the compiler that there is one variable available with the given type and name so that compiler can proceed for further compilation without requesting the compelete deatils of the variable.

**Dynamic type variable declaration:** A dynamic type variable declaration requires the compiler to interpret the type of the variable based on the vaule passed to it. The compiler does not require a variable  to have type statically as a necessary requirement. 


**syntax for variable**
```
var name type
```
**data type**
- Basic Types
    - Integers
        - Signed
            - int
            - int8
            - int16 
            - int32 
            - int64
        - Unsigned
            - uint
            - uint8
            - uint16
            - uint32
            - uint64
            - uintptr
    - Floats
        - float32
        - float64 (Common to use)
    - Complex Numbers (Imaginary Part for maths)
        - complex64
        - complex128
    - Byte
    - Rune
    - String
    - Boolean
- Composite Types
    - Collection/Aggregation or Non-Reference Types
        - Arrays
        - Structs
    - Reference Types
        - Slices
        - Maps
        - Channels
        - Pointers
        - Function/Methods
    -Interface
        - Special case of empty Interface

# Chapter Explicit and implicit decleration 

**Explicit:** I have delcare the variable type what is you should be and not override by the machine.
```
var number uint16 = 260
```

**Implicit:** I let the machine make the choice what variable type it was to declare it.
```
var number = 10
```

**expression asigned opertor:** We are skipping the "*var*" and let **GO** choose what type it is.
```
name := 239
```