#For Loop & List 
#list is a datatype to store multiple items in a single variable
calculation_to_units = 24
name_of_unit = "hours"

def days_to_units(num_of_days): 
        return f"{num_of_days} days are {num_of_days * calculation_to_units } {name_of_unit}"
   
def validate_and_execute():            #we have encapsulated the logic in the fuction 
    try:    
        user_input_number = int(user_input)
        if user_input_number> 0:                         #netsted if 
            calculated_value = days_to_units(user_input_number)
            print(calculated_value)
        
        elif user_input_number ==0:
            return "you entered a 0, please enter a valid positive number"
        
        else:
            print("You entered a negative number, no conversion for you!!")


    except ValueError:
        print ("your input is not a number. Don't ruin my program")

 #it will keep excecuting for "n" number of time if while True:

user_input =""             #as user_input is not defined 
while user_input != "exit":      #if the user_input is not (not equal too !=) exit than it does the loop        
    user_input = input("Hey user, enter a number of days and I will convert it to hours!\n") #it's a goble variable 
    validate_and_execute()
     