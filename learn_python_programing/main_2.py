#user Input
#input() - will ask the user for an input. python stops executing when it comes to the input(), it's a built-in function

user_input = input("Hey user, enter a number of days and i will convert it to hours!!\n") #assigning a variable to store the user inputs
#Expression is an intruction that combines values and operators and always evaluates down to single value


#Funcdtion with return values
#return values A function can return data as a result
calculation_to_units = 24
name_of_unit = "hours"

def days_to_units(num_of_days):                              #function is executed 
    return f"{num_of_days} days are {num_of_days * calculation_to_units} {name_of_unit}" #return value is assigned to variable

##my_var = days_to_units(20)        
##print(my_var)               #variable value is print to standard output
user_input_number = int(user_input)   #casting, if we don't cast python will by default take it as a string value not as a number. Converting the specified value into an integer number 

calculated_value =days_to_units(user_input_number) #taking user input and passing it to functions
print(calculated_value)