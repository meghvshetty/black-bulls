#str & int
print("200 is a great number") #string
print(20) #Intger
print(3.56) #float

#working with number
#Arithmetic Operator
print(20 * 24 * 60) #calc minutes in 20 days

#string concatenation (Adding strings together)
print("20 days are " + str(50) + " minutes")
print(f"20 days are {20*24*60} minutes")  #f stands for format 

#variables are containers for storing values
calculation_to_seconds = 24*60*60  #python is dynamically typed. you don't have to assign a type for variable 
print(f"20 day are {20 * calculation_to_seconds} seconds")

calculation_to_unit = 24*60*60
name_of_unit ="seconds"
print(f"20 days are {20*calculation_to_unit} {name_of_unit}") #best practice "use descriptive variable name" 

#functions (used to avoid repeating the same logic) 
def day_to_units():  #a function is defined using the def keyword.functions is a block of code which only runs when it is called 
    print(f"20 days are {20*calculation_to_unit} {name_of_unit}")  
    print("All good !!")

#calling a function = to execute the function
day_to_units()

#function parameters
    #information can be passed into functions as parameters
    #parameters are also called arguments
def days_to_units(number_of_days, custom_message): 
    print(f"{number_of_days} days are {number_of_days * calculation_to_unit}{name_of_unit}") #taking in the input parameter number_of_days
    print (custom_message)

days_to_units(30, "Hello")     #passing a input parameter
days_to_units(20, "darkness")     #clean code rather than having multiple print staments
days_to_units(10, "my old friend")

#scope a variable is only available from inside the region it is created.
# -global scope = variable available from within any scope
# -local scope = variable created inside function
global_variable="global"           #this is a  global variable which is created outside the function yet can be accesed within in the function.
def scope_check(local_variable):   #this is a local variable which is created and only accessed only within the function.
    print(local_variable)          #this is called the function body, you can create whatever logic you want inside the function body
    print(global_variable) 

scope_check("local") #calling the function

#creating variable inside a function
def create_variable():
    my_var = "variable inside the fuction"     #variable is defined inside the fuction. we don't have to pass any parameter
    print(my_var)

create_variable()