#vaildating user input is a good programing pratice 
# validate User input 
    # which doesn't make sense
    # that could cash our program
    # could even be a security risk 


#conditionals; expressions that evaluate to either true or false 
calculation_to_units = 24
name_of_unit = "hours"

def days_to_units(num_of_days):
    condition_check = num_of_days > 0
    print(type(condition_check))             #check what type of input it is  # there are two fuction call which is nested print() and type()
    
    if num_of_days > 0:                      #vailddating user input for negitive number 
        return f"{num_of_days} days are {num_of_days * calculation_to_units } {name_of_unit}"
    elif num_of_days ==0:
        return "you entered a 0, please enter a valid positive number"
    else:
        return "you entered a negative value, so no conersion for you!"

user_input = input("Hey user, enter a number of days and I will convert it to hours!\n")

#vaild user input (text or float)
if user_input.isdigit():
    user_input_number = int(user_input)
    calculated_value = days_to_units(user_input_number)
    print(calculated_value)

else:
    print ("your input is not a number. Don't ruin my program")
