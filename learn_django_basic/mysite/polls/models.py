from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.
#Here, each model is represented by a class that subclasses django.db.models.Model
#Each model has a number of class variables, each of which represents a database field in the model.
class Question (models.Model):                             
    question_text = models.CharField(max_length= 200) #Each field is represented by an instance of a Field class 
    pub_date = models.DateField('date published')     #This tells Django what type of data each field holds.

class Choice (models.Model):
    question = models.ForeignKey(Question, on_delete= models.CASCADE) #that tells Django each Choice is related to a single Question.Django supports all the common database relationships: many-to-one, many-to-many, and one-to-one.
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)